/*
	Database - is an organized collection of information or data.

	*Data - is raw and does not carry any specific meaning.

	*Information - is a group of organized data that contains logical meaning.

		Databases typically refer to information stored in a computer system but a database is also refer to physical databases.

			One such example is a 'library' which is a database of books.

		A database is managed by what is called a database management system.

			A DBMS is a system specifically designed to manage the storage, retreaval and modification of data in a database.

				*Add
				*Edit
				*Delete

			DBMS of a physical library is 'Dewey Decimal System'.

		Databased systems allow four types of operation when in comes to handling data, namely, create(insert), read(select), update and delete.

		Collectively if is called CRUD Operation.

	Types o Database System
	
	Relational database is a type of data base where data is stored as a set of tables with rows and columns(pretty much like a table printed on a piece of paper)

	For unstructured data (data that cannot fit into a strict tabular format), NoSQL databases are commonly used.

	What is SQL?
	Sql stands for structured query language. it is the language used typically in relational DBMS to store, retreve and modify data.


	What is NoSQL?

	mean not only SQL was conceptualized when capturing cpmplex, unstructured data became more dificult.

	One of the popular NoSQL databases in MongoDB.

	What is mongoDB?

	is and open-source database and the leading NoSql database.
	its language is highly expressive, and generally friendly to those already familiar with JSON structure.

	Mongo in MongoDB is a part of the word humongous which then means huge or enormous.

	The main advantage of the data structure is that is is stored in mongoDB using the JSON format.

	Also, in MongoDB, some terms in relation databases will be changed:

	*from tables to collection;
	*rows to documents for rows; and 
	*from columns to fields
*/

//[SECTION] Data Modeling

//1. Identify what information we want to gather from the customers in order to determine wether the user's identity is true. 


	//=> WHY? 
   //1. TO PROPERLY PLAN out what information will be deemed useful.
   //2. to lessen the chances or scenarios of having to modify or edit the data stored in the database.
   //3. to anticipate how this data/information would relate to each other.

	//TASK: Create a Course Booking System for an Institution. 

     //What are the 'minimum information' would i need to collect from the customers/students. 
     //the following information described below would identify the structure of the user in our app
     
     //User Documents
	     // 1. first Name
	     // 2. last Name
	     // 3. Middle Name
	     // 4. email address
	     // 5. PIN/password
	     // 6. mobile number 
	     // 7. birthdate 
	     // 8. gender
	     // 9. isAdmin: role and restriction/limitations that this user would have in our app. 
	     //10. dateTimeRegistered => when the student signed up/enrolled in the institution. 

	//Course/Subjects 

	    //1. name/title
	    //2. course code 
	    //3. course description
	    //4. course units
	    //5. course instructor
	    //6. isActive => to describe if the course is being offered by the institution. 
	    //7. dateTimeCreated => for us to identify when the course was added to the database. 
	    //8. available slots


     //As a full stack web developer (front, backend, database)
     //The more information you gather the more difficult it is to scale and manage the collection. it is going more difficult to maintain. 



//2. Create an ERD to represent the entities inside the database as well as to describe the relationships amongst them. 


     //users can have multiple subjects
     //subjects can have multiple enrollees

     //user can have multiple transactions
     //transaction can only belong to a single user. 

     //a single transaction can have multiple courses
     //a course can be part of mutiple transactions 
